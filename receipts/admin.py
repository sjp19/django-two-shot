from django.contrib import admin
from .models import (
    ExpenseCategory,
    Account,
    Receipt,
)

class ReceiptAdmin(admin.ModelAdmin):
    pass
class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass
class AccountAdmin(admin.ModelAdmin):
    pass


# Register your models here.
admin.site.register(ExpenseCategory, ExpenseCategoryAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(Receipt, ReceiptAdmin)
