from django.contrib import admin
from django.urls import path

from receipts.views import (
    receipt_list,
    create_receipt,
    expenses_list,
    accounts_list,
    create_expense,
    create_account_view,
)

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", expenses_list, name="expenses_list"),
    path("accounts/", accounts_list, name="list_accounts"),
    path("categories/create/", create_expense, name="create_category"),
    path("accounts/create/", create_account_view, name="create_account"),
]
