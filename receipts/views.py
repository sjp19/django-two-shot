from django.shortcuts import (
    render,
    redirect,
)

from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from .models import (
    ExpenseCategory,
    Account,
    Receipt,
)
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def receipt_list(request):
    receipt_list_info = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipt_list_info}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def expenses_list(request):
    expense_categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expensecategory_list": expense_categories,
    }
    return render(request, "expense_categories/list.html", context)


@login_required
def accounts_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts_list": accounts,
    }
    return render(request, "accounts/list.html", context)


@login_required
def create_expense(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(commit=False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("expenses_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "expense_categories/create.html", context)


@login_required
def create_account_view(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("list_accounts")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
